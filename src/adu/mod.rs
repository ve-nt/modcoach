pub mod ascii;
pub mod rtu;

/// Holds all the necessary functionality to interface to a ADU variant.
pub trait Adu {
    fn frame(&self) -> Vec<u8>;
}
