use crate::args::Args;
use crate::modbus::Modbus;
use crate::Adu;
use byteorder::{BigEndian, WriteBytesExt};

/// The ascii implementation for modbus. Designed to be sent over a serial line
/// as a stream of chars in this order:
///
/// `[char; 2]` `slave_id`: The ID of the slave you wish to talk to.
/// `[char; 2]` `function_code`: The function code you desire to use.
/// `[char; 4]` `register`: The starting register to read from/write to.
/// `[char; 4]` `quantity`: How many registers you should read/write.
/// `[char; 2]` `crc`: Redundancy check.
///
/// A complete frame will contain all these fields as a byte array, so it will
/// convert each char into a byte. No endian-ness as it is working with ascii.
pub struct Ascii {
    modbus: Modbus,
    slave_id: u8,
}

impl Ascii {
    pub fn new(slave_id: u8, modbus: Modbus) -> Self {
        Self { slave_id, modbus }
    }

    pub fn from_args(args: &Args) -> Self {
        let slave_id = args.slave_id;
        let modbus = args.modbus();

        Self { slave_id, modbus }
    }

    /// Get a modbus frame as an array of bytes, without redundancy check.
    fn bytes(&self) -> Vec<u8> {
        let mut bytes: Vec<u8> = Vec::new();

        bytes.push(self.slave_id);
        bytes.push(self.modbus.function_code);
        bytes.write_u16::<BigEndian>(self.modbus.register).unwrap();
        bytes.write_u16::<BigEndian>(self.modbus.quantity).unwrap();
        bytes
    }

    /// Returns the redundancy check, using the modbus ascii LRC algorithm.
    fn redundancy_check(&self) -> u8 {
        lrc(&self.bytes())
    }
}

impl Adu for Ascii {
    fn frame(&self) -> Vec<u8> {
        let mut frame: Vec<u8> = Vec::new();

        // Start by pushing the header character.
        frame.push(b':');

        // Push all the details of our struct.
        for byte in &self.bytes() {
            for c in byte_to_padded_hex(*byte).iter() {
                frame.push(*c as u8);
            }
        }

        // Push the redundancy check.
        for c in &byte_to_padded_hex(self.redundancy_check()) {
            frame.push(*c as u8);
        }

        // Push the trailer characters.
        const END_CHARS: [char; 2] = ['\r', '\n'];
        for end_char in &END_CHARS {
            frame.push(*end_char as u8);
        }

        frame
    }
}

/// Converts a byte to an array of it's hex represenation. If the byte value is
/// less than the minimum value of 2 nibbles (0x10) it will left-pad the value
/// with a zero.
///
///```
///assert_eq!(byte_to_padded_hex(0x0F), ['0', 'F']);
///```
fn byte_to_padded_hex(byte: u8) -> [char; 2] {
    let hex: Vec<char> = format!("{:x}", byte)
        .chars()
        .map(|c| c.to_ascii_uppercase())
        .collect();

    if hex.len() < 2 {
        ['0', hex[0]]
    } else {
        [hex[0], hex[1]]
    }
}

/// Calculates the longitudinal redundancy check of a series of bytes, used by
/// modbus ascii to check for packet loss.
fn lrc(bytes: &[u8]) -> u8 {
    let mut n_lrc: u8 = 0;

    for byte in bytes {
        n_lrc = n_lrc.wrapping_add(*byte);
    }

    0u8.wrapping_sub(n_lrc)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_lrc() {
        assert_eq!(lrc(&[0x11, 0x10, 0x00, 0x01, 0x00, 0x02]), 0xDC);
        assert_eq!(lrc(&[0x11, 0x03, 0x00, 0x6B, 0x00, 0x03]), 0x7E);
    }

    #[test]
    fn test_byte_to_padded_ascii() {
        assert_eq!(byte_to_padded_hex(0x38), ['3', '8']);
        assert_eq!(byte_to_padded_hex(0x0F), ['0', 'F']);
        assert_eq!(byte_to_padded_hex(0x00), ['0', '0']);
        assert_eq!(byte_to_padded_hex(0xFF), ['F', 'F']);
    }

    #[test]
    fn test_ascii_bytes() {
        let modbus = Modbus::new(4, 2, 8);
        let ascii = Ascii::new(1, modbus);
        assert_eq!(ascii.bytes(), vec![0x01, 0x04, 0x00, 0x02, 0x00, 0x08]);
    }

    #[test]
    fn test_ascii_redundancy_check() {
        let modbus = Modbus::new(0x10, 0x01, 0x02);
        let ascii = Ascii::new(0x11, modbus);
        assert_eq!(ascii.redundancy_check(), 0xDC);

        let modbus = Modbus::new(0x03, 0x6B, 0x03);
        let ascii = Ascii::new(0x11, modbus);
        assert_eq!(ascii.redundancy_check(), 0x7E);
    }

    #[test]
    fn test_ascii_frame() {
        let modbus = Modbus::new(0x10, 0x01, 0x02);
        let ascii = Ascii::new(0x11, modbus);
        assert_eq!(
            ascii.frame(),
            vec![
                0x3A, 0x31, 0x31, 0x31, 0x30, 0x30, 0x30, 0x30, 0x31, 0x30, 0x30, 0x30, 0x32, 0x44,
                0x43, 0x0D, 0x0A
            ]
        );
    }
}
