use crate::args::Args;
use crate::modbus::Modbus;
use crate::Adu;
use byteorder::{BigEndian, LittleEndian, WriteBytesExt};

/// The RTU implementation for modbus. Designed to be sent over a serial line as
/// a stream of bytes in this order:
///
/// `u8` `slave_id`: The ID of the slave you wish to talk to.
/// `u8` `function_code`: The function code you desire to use.
/// `u16` `register`: The starting register to read from/write to.
/// `u16` `quantity`: How many registers you should read/write.
/// `u16` `crc`: Redundancy check.
///
/// A complete frame will contain all these fields as a byte array. All fields
/// are big endian, except for the CRC which is little endian.
pub struct Rtu {
    modbus: Modbus,
    slave_id: u8,
}

impl Rtu {
    pub fn new(slave_id: u8, modbus: Modbus) -> Self {
        Self { slave_id, modbus }
    }

    pub fn from_args(args: &Args) -> Self {
        let slave_id = args.slave_id;
        let modbus = args.modbus();

        Self { slave_id, modbus }
    }

    /// Get a modbus frame as an array of bytes, without redundancy check.
    fn bytes(&self) -> [u8; 6] {
        let mut bytes: [u8; 6] = [0; 6];

        // Split the register and quantity (both u16) into a vector of 2 bytes
        // each. (big endian).
        let mut register_array = vec![];
        register_array
            .write_u16::<BigEndian>(self.modbus.register)
            .unwrap();

        let mut quantity_array = vec![];
        quantity_array
            .write_u16::<BigEndian>(self.modbus.quantity)
            .unwrap();

        // Form the byte array.
        bytes[0] = self.slave_id;
        bytes[1] = self.modbus.function_code;
        bytes[2] = register_array[0];
        bytes[3] = register_array[1];
        bytes[4] = quantity_array[0];
        bytes[5] = quantity_array[1];
        bytes
    }

    /// Generate the CRC of a modbus struct. Note that a CRC on a modbus frame
    /// is little endian, unlike the rest of the fields.
    fn redundancy_check(&self) -> [u8; 2] {
        let mut crc: [u8; 2] = [0; 2];
        let mut crc_vec = vec![];
        crc_vec
            .write_u16::<LittleEndian>(crc16(&self.bytes()))
            .unwrap();

        for (i, byte) in crc_vec.into_iter().enumerate() {
            crc[i] = byte;
        }
        crc
    }
}

impl Adu for Rtu {
    /// Generate the complete frame for a modbus RTU request message. This is a
    /// modbus message converted to an array of bytes, with a 2 byte CRC (used
    /// as the redundancy check) appended on the end.
    fn frame(&self) -> Vec<u8> {
        let mut frame: Vec<u8> = Vec::new();

        // Fill the frame with the modbus bytes
        for byte in &self.bytes() {
            frame.push(*byte);
        }

        // Append the redundancy check to the frame
        for byte in &self.redundancy_check() {
            frame.push(*byte);
        }

        frame
    }
}

/// Calculates Modbus CRC16, which is used by modbus packets to check for packet
/// loss. Returns a CRC in the form of a 16 bit integer (little endian).
fn crc16(bytes: &[u8]) -> u16 {
    let mut crc: u16 = 0xFFFF;

    for byte in bytes {
        crc ^= u16::from(*byte);

        for _ in (1..=8).rev() {
            if (crc & 0x0001) != 0 {
                crc >>= 1;
                crc ^= 0xA001;
            } else {
                crc >>= 1;
            }
        }
    }
    crc
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_rtu_bytes() {
        // Test a regular modbus message.
        let modbus = Modbus::new(3, 4, 1);
        let rtu = Rtu::new(2, modbus);
        assert_eq!(rtu.bytes(), [2, 3, 0, 4, 0, 1]);

        // Test max values for modbus messages.
        let modbus = Modbus::new(u8::max_value(), u16::max_value(), u16::max_value());
        let rtu = Rtu::new(u8::max_value(), modbus);
        assert_eq!(rtu.bytes(), [u8::max_value(); 6]);
    }

    #[test]
    fn test_rtu_crc16() {
        let modbus_message = vec![
            0x02, // slave id
            0x03, // function code
            0x00, // start register[0]
            0x04, // start register[1]
            0x00, // number of words[0]
            0x01, // number of words[1]
        ];
        assert_eq!(crc16(&modbus_message), 0xF8C5);

        let message = [0x02, 0x03, 0x00, 0x04];
        assert_eq!(crc16(&message), 0x5FF0);

        let message = [0x01, 0x23, 0x45, 0x67, 0x89];
        assert_eq!(crc16(&message), 0x07E8);

        let message = [0x01];
        assert_eq!(crc16(&message), 0x807E);

        let message = [0x00];
        assert_eq!(crc16(&message), 0x40BF);

        let message = [
            0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0x11, 0x22, 0x33, 0x44, 0x55,
            0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0x11, 0x22, 0x33, 0x44, 0x55,
            0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0x11, 0x22, 0x33, 0x44, 0x55,
            0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0x11, 0x22, 0x33, 0x44, 0x55,
        ];
        assert_eq!(crc16(&message), 0xE80F);
    }

    #[test]
    fn test_rtu_redundancy_check() {
        let modbus = Modbus::new(4, 8, 2);
        let rtu = Rtu::new(3, modbus);
        assert_eq!(rtu.redundancy_check(), [0xF1, 0xEB]);
    }

    #[test]
    fn test_rtu_frame() {
        let modbus = Modbus::new(3, 0, 2);
        let rtu = Rtu::new(1, modbus);
        assert_eq!(rtu.frame(), vec![0x1, 0x3, 0x0, 0x0, 0x0, 0x2, 0xC4, 0x0B])
    }
}
