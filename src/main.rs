use adu::Adu;
use args::Args;
use comm::Comm;

mod adu;
mod args;
mod comm;
mod modbus;

const PKG_NAME: &str = env!("CARGO_PKG_NAME");

fn main() {
    // Get the arguments passed from the CLI
    let args = Args::get();

    let mut modcoach = ModCoach::from_args(&args);
    modcoach.send();
}

/// Gathers all the functionality we need for the program to carry out it's job.
struct ModCoach {
    /// A variant of the modbus protocol, e.g. rtu, ascii, TCP, etc...
    adu: Box<dyn Adu>,

    /// A method of communicating that protocol, e.g. serial, TCP, etc...
    comm: Box<dyn Comm>,
}

impl ModCoach {
    /// Gather up the ADU and comm type that the user has specified.
    fn from_args(args: &Args) -> Self {
        // Set the ADU specified by the user.
        let adu: Box<dyn Adu> = match args.adu() {
            args::Adu::Rtu => Box::new(adu::rtu::Rtu::from_args(&args)),
            args::Adu::Ascii => Box::new(adu::ascii::Ascii::from_args(&args)),
        };

        // Infer the communication type from the ADU specified.
        let comm: Box<dyn Comm> = match args.adu() {
            args::Adu::Rtu => Box::new(comm::serial::Serial::from_args(&args)),
            args::Adu::Ascii => Box::new(comm::serial::Serial::from_args(&args)),
        };

        Self { adu, comm }
    }

    /// Send the frame from the ADU type over the comm type.
    fn send(&mut self) {
        self.comm.send(&self.adu.frame());
    }
}
