use serialport::{DataBits, Parity, StopBits};
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(rename_all = "kebab", author = "")]
pub struct SerialArgs {
    /// Serial device to send modbus queries over
    #[structopt(short = "d", long = "device", parse(from_os_str))]
    pub device: PathBuf,

    /// Serial parameters: <data-bits><parity><stop-bits>
    #[structopt(
        short = "S",
        long = "serial-parameters",
        default_value = "8N1",
        name = "paramaters"
    )]
    serial_parameters: String,

    /// Baudrate to use for serial devices
    #[structopt(short = "B", long = "baudrate", default_value = "9600")]
    baudrate: u32,

    /// Flow control for serial devices
    #[structopt(short = "F", long = "flow-control", default_value = "None")]
    flow_control: String,

    /// Timeout duration (milliseconds)
    #[structopt(short = "T", long = "timeout", default_value = "1000")]
    timeout: u64,
}

impl SerialArgs {
    /// Return the full serialport settings.
    pub fn serial_settings(&self) -> serialport::SerialPortSettings {
        let serial_params = self.serial_parameters().unwrap();

        serialport::SerialPortSettings {
            baud_rate: self.baudrate,
            data_bits: serial_params.0,
            parity: serial_params.1,
            stop_bits: serial_params.2,
            flow_control: self.flow_control(),
            timeout: std::time::Duration::from_millis(self.timeout),
        }
    }

    /// Gets a tuple of serialport parameters passed via command-line arguments.
    fn serial_parameters(&self) -> Result<(DataBits, Parity, StopBits), ParseSerialError> {
        let s: Vec<char> = self.serial_parameters.to_string().chars().collect();

        if s.len() != 3 {
            return Err(ParseSerialError::IncorrectLength);
        }

        let data_bits = match s[0] {
            '5' => DataBits::Five,
            '6' => DataBits::Six,
            '7' => DataBits::Seven,
            '8' => DataBits::Eight,
            _ => return Err(ParseSerialError::BadDataBits(s[0])),
        };

        let parity = match s[1] {
            'N' | 'n' => Parity::None,
            'E' | 'e' => Parity::Even,
            'O' | 'o' => Parity::Odd,
            _ => return Err(ParseSerialError::BadParity(s[1])),
        };

        let stop_bits = match s[2] {
            '1' => StopBits::One,
            '2' => StopBits::Two,
            _ => return Err(ParseSerialError::BadStopBits(s[2])),
        };

        Ok((data_bits, parity, stop_bits))
    }

    /// Checks if serial parameters passed by the user are valid.
    pub fn check_serial_parameters(&self) {
        use ParseSerialError::{BadDataBits, BadParity, BadStopBits, IncorrectLength};

        if self.serial_parameters().is_err() {
            eprint!("{}: Could not parse serial parameters. ", crate::PKG_NAME);

            match self.serial_parameters().err().unwrap() {
                IncorrectLength => eprintln!("Incorrect serial parameters."),
                BadDataBits(d) => eprintln!("'{}' is not a valid data bits setting.", d),
                BadParity(p) => eprintln!("'{}' is not a valid parity setting.", p),
                BadStopBits(s) => eprintln!("'{}' is not a valid stop bits setting.", s),
            }

            eprintln!(
                "{}: Format required: `<data-bits><parity><stop-bits>` \
                 Example: -S 8N1",
                crate::PKG_NAME
            );

            std::process::exit(1);
        }
    }

    /// Gets serialport flow control type passed via command-line arguments.
    pub fn flow_control(&self) -> serialport::FlowControl {
        use serialport::FlowControl;

        match self.flow_control.to_lowercase().as_str() {
            "none" => FlowControl::None,
            "hardware" => FlowControl::Hardware,
            "software" => FlowControl::Software,
            _ => {
                eprintln!(
                    "{}: Did not recognise flow control type '{}'",
                    crate::PKG_NAME,
                    self.flow_control
                );
                std::process::exit(1);
            }
        }
    }
}

#[derive(PartialEq, Debug)]
pub enum ParseSerialError {
    IncorrectLength,
    BadDataBits(char),
    BadParity(char),
    BadStopBits(char),
}

#[cfg(test)]
mod tests {
    use super::super::Args;
    use super::*;

    #[test]
    fn test_serial_parameters() {
        let mut args = Args {
            verbose: false,
            slave_id: 1,
            function_code: 3,
            register: 0,
            quanitiy: 8,
            adu: "rtu".to_string(),
            serial: SerialArgs {
                device: PathBuf::from("/dev/pts/7"),
                serial_parameters: "8N1".to_string(),
                baudrate: 9600,
                flow_control: "None".to_string(),
                timeout: 1000,
            },
        };

        args.serial.serial_parameters = "8N1".to_string();
        assert_eq!(
            args.serial.serial_parameters().unwrap(),
            (DataBits::Eight, Parity::None, StopBits::One)
        );

        args.serial.serial_parameters = "1N1".to_string();
        assert_eq!(
            args.serial.serial_parameters(),
            Err(ParseSerialError::BadDataBits('1'))
        );

        args.serial.serial_parameters = "7H1".to_string();
        assert_eq!(
            args.serial.serial_parameters(),
            Err(ParseSerialError::BadParity('H'))
        );

        args.serial.serial_parameters = "7Oi".to_string();
        assert_eq!(
            args.serial.serial_parameters(),
            Err(ParseSerialError::BadStopBits('i'))
        );

        args.serial.serial_parameters = "".to_string();
        assert_eq!(
            args.serial.serial_parameters(),
            Err(ParseSerialError::IncorrectLength)
        );
    }

    #[test]
    fn test_serial_flow_control() {
        use serialport::FlowControl;

        let mut args = Args {
            verbose: true,
            slave_id: 1,
            function_code: 3,
            register: 0,
            quanitiy: 8,
            adu: "rtu".to_string(),
            serial: SerialArgs {
                device: PathBuf::from("/dev/pts/7"),
                serial_parameters: "7E2".to_string(),
                baudrate: 115200,
                flow_control: "software".to_string(),
                timeout: 3000,
            },
        };
        assert_eq!(args.serial.flow_control(), FlowControl::Software);

        args.serial.flow_control = "hardware".to_string();
        assert_eq!(args.serial.flow_control(), FlowControl::Hardware);

        args.serial.flow_control = "none".to_string();
        assert_eq!(args.serial.flow_control(), FlowControl::None);
    }

    #[test]
    fn test_serial_settings() {
        let mut args = Args {
            verbose: true,
            slave_id: 1,
            function_code: 3,
            register: 0,
            quanitiy: 8,
            adu: "rtu".to_string(),
            serial: SerialArgs {
                device: PathBuf::from("/dev/pts/7"),
                serial_parameters: "7E2".to_string(),
                baudrate: 115200,
                flow_control: "software".to_string(),
                timeout: 3000,
            },
        };

        assert_eq!(
            "SerialPortSettings { \
             baud_rate: 115200, \
             data_bits: Seven, \
             flow_control: Software, \
             parity: Even, \
             stop_bits: Two, \
             timeout: 3s \
             }"
            .to_string(),
            format!("{:?}", args.serial.serial_settings())
        );

        args.serial.flow_control = "hardware".to_string();
        args.serial.baudrate = 57600;
        args.serial.serial_parameters = "5O1".to_string();
        args.serial.timeout = 10_000;
        assert_eq!(
            "SerialPortSettings { \
             baud_rate: 57600, \
             data_bits: Five, \
             flow_control: Hardware, \
             parity: Odd, \
             stop_bits: One, \
             timeout: 10s \
             }"
            .to_string(),
            format!("{:?}", args.serial.serial_settings())
        );
    }
}
