use serial::SerialArgs;
use structopt::StructOpt;

mod serial;

// Holds all the command-line arguments that can be passed to the
// program. Fields are filled with `Args::get()`.
#[derive(StructOpt, Debug)]
#[structopt(rename_all = "kebab", author = "")]
pub struct Args {
    /// Verbose output
    #[structopt(short = "v", long = "verbose")]
    pub verbose: bool,

    /// Slave ID
    #[structopt(parse(try_from_str))]
    pub slave_id: u8,

    /// Function code
    #[structopt(parse(try_from_str))]
    pub function_code: u8,

    /// Starting register
    #[structopt(parse(try_from_str))]
    pub register: u16,

    /// Number of registers to request
    #[structopt(parse(try_from_str))]
    pub quanitiy: u16,

    /// Adu type to use
    #[structopt(short = "t", long = "adu", parse(try_from_str))]
    adu: String,

    #[structopt(flatten)]
    pub serial: SerialArgs,
}

impl Args {
    /// Parses command-line arguments and returns all valid ones in a
    /// struct using the structopt crate.
    pub fn get() -> Args {
        let args = Args::from_args();

        args.serial.check_serial_parameters();
        args.serial.flow_control();
        args.serial.serial_settings();
        args.adu();

        args
    }

    pub fn modbus(&self) -> crate::modbus::Modbus {
        crate::modbus::Modbus {
            function_code: self.function_code,
            register: self.register,
            quantity: self.quanitiy,
        }
    }

    pub fn adu(&self) -> Adu {
        match self.adu.to_lowercase().as_str() {
            "rtu" => Adu::Rtu,
            "ascii" => Adu::Ascii,
            _ => {
                eprintln!("Did not recognise adu type '{}'", self.adu);
                std::process::exit(1);
            }
        }
    }
}

pub enum Adu {
    Rtu,
    Ascii,
}
