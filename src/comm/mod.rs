pub mod serial;

pub trait Comm {
    fn send(&mut self, message: &[u8]) -> usize;
}
