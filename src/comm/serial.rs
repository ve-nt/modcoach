use super::Comm;
use crate::args::Args;
use serialport::posix::TTYPort;
use serialport::SerialPortSettings;
use std::io::Write;
use std::path::{Path, PathBuf};

pub struct Serial {
    port: PathBuf,
    tty: TTYPort,
}

impl Serial {
    /// Opens up a serial connection, looking at the arguments the user passed
    /// to the program.
    pub fn from_args(args: &Args) -> Self {
        let port = &args.serial.device;
        let settings = &args.serial.serial_settings();

        let tty = Serial::open(&port, &settings);

        Self {
            port: port.to_path_buf(),
            tty,
        }
    }

    /// Writes to the TTYport
    pub fn write(&mut self, message: &[u8]) -> usize {
        let written: usize = match self.tty.write(message) {
            Ok(s) => s,
            Err(e) => {
                eprintln!(
                    "{}: {}: {}",
                    crate::PKG_NAME,
                    self.port.to_str().unwrap(),
                    e
                );
                std::process::exit(e.raw_os_error().unwrap())
            }
        };
        self.tty.flush().unwrap();

        written
    }

    /// Opens up a connection to a TTY port.
    fn open(device: &Path, settings: &SerialPortSettings) -> TTYPort {
        let port = TTYPort::open(&device, settings);

        match port {
            Ok(p) => p,
            Err(e) => {
                handle_serial_error(e, &device);
                std::process::exit(1);
            }
        }
    }
}

impl Comm for Serial {
    fn send(&mut self, message: &[u8]) -> usize {
        self.write(message)
    }
}

/// Output an error message caused by failing to open a TTY port.
fn handle_serial_error(e: serialport::Error, path: &Path) {
    use serialport::ErrorKind::*;

    let path_str = path.to_str().unwrap();
    const PKG_NAME: &str = crate::PKG_NAME;

    if !path.exists() {
        eprintln!("{}: {}: No such file or directory", PKG_NAME, path_str);
        return;
    }

    match e.kind {
        NoDevice => eprintln!("{}: {}: {}", PKG_NAME, path_str, e.description),
        InvalidInput => eprintln!("{}: {}: Invalid input", PKG_NAME, path_str),
        Unknown => eprintln!("{}: {}: {}", PKG_NAME, path_str, e.description),
        Io(e) => handle_io_error(e, path),
    }
}

/// Output an error message for std::io releated errors and exit with a related
/// error code.
fn handle_io_error(e: std::io::ErrorKind, path: &Path) {
    let error = std::io::Error::from(e);
    let error_message = error.to_string();

    match path.to_str() {
        Some(p) => eprintln!("{}: {}: {}", crate::PKG_NAME, p, error_message),
        None => eprintln!("{}: {}", crate::PKG_NAME, error_message),
    }

    std::process::exit(error.raw_os_error().unwrap_or(1));
}
