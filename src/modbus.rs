/// Holds all the essential information for a modbus packet.
///
/// `u8` `function_code`: The function code you desire to use.
/// `u16` `register`: The starting register to read from/write to.
/// `u16` `quantity`: How many registers you should read/write.
///
/// This lacks any ADU information. It will need to be wrapped around a
/// ADU type in order to be used.
pub struct Modbus {
    pub function_code: u8,
    pub register: u16,
    pub quantity: u16,
}

impl Modbus {
    pub fn new(function_code: u8, register: u16, quantity: u16) -> Self {
        Modbus {
            function_code,
            register,
            quantity,
        }
    }
}
